function updateBib(item) {
	let name = document.getElementById('name-input').value
	let num = document.getElementById('number-input').value
	
	if ( name.toLowerCase().includes('scott') ) {
		num = '<span class="squid">&#129425;&#129425;&#129425;</span>';
	}

	document.getElementById('name-output').innerHTML = name;
	document.getElementById('number-output').innerHTML = num;
}